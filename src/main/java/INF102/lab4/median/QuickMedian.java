package INF102.lab4.median;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;


public class QuickMedian implements IMedian {

    @Override
    public <T extends Comparable<T>> T median(List<T> list) {
        
        List<T> listCopy = new ArrayList<>(list);

        QuickSelect quickSelect = new QuickSelect();
        return (T) quickSelect.findMedian(listCopy); 
        

    }

}
