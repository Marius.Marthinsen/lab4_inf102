package INF102.lab4.median;

import java.util.List;
import java.util.Random;

// Code from ChatGPT 
// Prompt: can you give me a quickselect code that instead uses a list containing elements T, I know that I need integers to do this I just want you to replace integers with T

public class QuickSelect<T extends Comparable<T>> {
    public T findMedian(List<T> list) {
        int n = list.size();
        int k = (n - 1) / 2; // Index of the median

        return quickSelect(list, 0, n - 1, k);
    }

    private T quickSelect(List<T> list, int left, int right, int k) {
        if (left == right) {
            return list.get(left);
        }

        int pivotIndex = partition(list, left, right);

        if (k == pivotIndex) {
            return list.get(k);
        } else if (k < pivotIndex) {
            return quickSelect(list, left, pivotIndex - 1, k);
        } else {
            return quickSelect(list, pivotIndex + 1, right, k);
        }
    }

    private int partition(List<T> list, int left, int right) {
        // Choose a random pivot element
        int randomIndex = left + new Random().nextInt(right - left + 1);
        swap(list, randomIndex, right);

        T pivot = list.get(right);
        int i = left;

        for (int j = left; j < right; j++) {
            if (list.get(j).compareTo(pivot) <= 0) {
                swap(list, i, j);
                i++;
            }
        }

        swap(list, i, right);
        return i;
    }

    private void swap(List<T> list, int i, int j) {
        T temp = list.get(i);
        list.set(i, list.get(j));
        list.set(j, temp);
    }

    public static void main(String[] args) {
        List<Integer> nums = List.of(3, 1, 7, 2, 8, 4, 5);

        QuickSelect<Integer> quickSelect = new QuickSelect<>();
        Integer median = quickSelect.findMedian(nums);
        System.out.println("Median: " + median);
    }
}