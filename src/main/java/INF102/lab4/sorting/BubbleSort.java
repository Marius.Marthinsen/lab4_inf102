package INF102.lab4.sorting;

import java.util.List;

public class BubbleSort implements ISort {

    @Override
    public <T extends Comparable<T>> void sort(List<T> list) { // O(n^2)
        for (int i = 0; i < list.size(); i++) { // O(n)
            for (int j = 1; j < list.size() - i; j++) { // O(n)
                if (list.get(j - 1).compareTo(list.get(j)) > 0) { // O(1)
                    T temp = list.get(j - 1); // O(1)
                    list.set(j - 1, list.get(j)); // O(1)
                    list.set(j, temp); // O(1)
                }
            }
        }
    }
    
}
